package pc.modelling

import scala.util.Random

trait CoreCTMC[A] {
  def nextWithRate(a: A): Set[(Double,A)]
}

trait CTMC[A] extends CoreCTMC[A] with System[A] {

  def next(a: A): Set[A] = nextWithRate(a) map (_._2)

  def nextWithRate(a: A): Set[(Double,A)]

  def simulation(a0: A, rnd: Random): Stream[(Double,A)] =
    Stream.iterate( (0.0,a0) ){ case (t,a) => {
      val next = nextWithRate(a).toList
      if (next.isEmpty) (t,a) else {
        val rates = next.map(_._1).sum
        val rnd1 = rnd.nextDouble()
        val cumulative = next.scanLeft((0.0, a0)) { case ((r,a),(r2,a2)) => (r+r2/rates,a2) }.tail
        val choice = cumulative.collect { case (p, a) if p >= rnd1 => a }.head
        //println(a,next,rates,cumulative,rnd1,choice)
        (t + Math.log(1 / rnd.nextDouble()) / rates, choice)
      }
    }}
}

object CTMC {

  def ofFunction[A](f: PartialFunction[A,Set[(Double,A)]]): CTMC[A] = new CoreCTMC[A] with CTMC[A]{
    override def nextWithRate(a: A) = f.applyOrElse(a,(x: A)=>Set[(Double,A)]())
  }

  def ofRelation[A](r: Set[(A,Double,A)]): CTMC[A] = ofFunction{ case a =>
    r filter {_._1 == a} map {t=>(t._2,t._3)}
  }

  def ofTransitions[A](r: (A,Double,A)*): CTMC[A] = ofRelation(r.toSet)
}

object TryCTMC extends App {
  object State extends Enumeration {
    val idle,send,done,fail = Value
  }
  import State._

  val channel: CTMC[State.Value] = CTMC.ofTransitions(
    (idle,1.0,send),
    (send,100000.0,send),
    (send,200000.0,done),
    (send,100000.0,fail),
    (fail,100000.0,idle),
    (done,1.0,done)
  )
  println(channel.simulation(idle, new Random()).take(10).toList.mkString("\n"))
}